﻿namespace ToolsOnDemand
{
    using System;
    using System.Linq;

    /// <summary>
    /// The program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main entry point for the software.
        /// </summary>
        public static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                Usage();
                return 1;
            }

            var spareargs = args.Skip(1).ToArray();

            if ((args[0].ToLower() == "help" && args.Length == 1)
                || args[0].ToLower() == "-help"
                || args[0].ToLower() == "/help"
                || args[0].ToLower() == "h"
                || args[0].ToLower() == "-h"
                || args[0].ToLower() == "/h"
                || args[0].ToLower() == "?"
                || args[0].ToLower() == "-?"
                || args[0].ToLower() == "/?")
            {
                return Usage();
            }

            switch (args[0].ToLowerInvariant())
            {
                case "help":
                    return Help(spareargs);
                case "install":
                    return Install(spareargs);
                case "update":
                    return Update(spareargs);
                default:
                Console.WriteLine("Command-line " + Environment.CommandLine + " not recognised.");
                Usage();
                return 1;
            }
        }

        private static int Help(string[] args)
        {
            return 0;
        }

        private static int Install(string[] args)
        {
            return 0;
        }

        private static int Update(string[] args)
        {
            return 0;
        }

        private static int Usage()
        {
            Console.WriteLine();
            return 0;
        }
    }
}

