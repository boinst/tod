@echo off

cd /d %~dp0

REM ### Postbuild task for TOD
REM ### Produces a combined C# file

if exist (..\..\tod.cs) del ..\..\tod.cs
powershell.exe -NoProfile -NonInteractive -ExecutionPolicy unrestricted -Command "Get-Content *.cs | Set-Content ..\..\tod.cs"

