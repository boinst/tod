﻿namespace ToolsOnDemand
{
    using Newtonsoft.Json;

    /// <summary>
    /// The app info.
    /// </summary>
    [JsonObject("app")]
    public class AppInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("license")]
        public string LicenseUri { get; set; }
    }
}