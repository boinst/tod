@echo off
setlocal enableextensions enabledelayedexpansion 
REM ### This script downloads and installs GNU Make for Windows
REM ### http://gnuwin32.sourceforge.net/packages/make.htm
REM ### The build is provided by the GnuWin project and SourceForce

:DOWNLOAD
REM ### download the files
if not exist "%~dp0.\pkgs\gnuwin32" mkdir "%~dp0.\pkgs\gnuwin32"
if not exist "%~dp0.\pkgs\gnuwin32\make-3.81-bin.zip" (
    echo Downloading GNU Make
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-download.ps1" "http://downloads.sourceforge.net/project/gnuwin32/make/3.81/make-3.81-bin.zip" "%~dp0.\pkgs\gnuwin32\make-3.81-bin.zip"
)
if not exist "%~dp0.\pkgs\gnuwin32\make-3.81-dep.zip" (
    echo Downloading GNU Make Dependencies
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-download.ps1" "http://downloads.sourceforge.net/project/gnuwin32/make/3.81/make-3.81-dep.zip" "%~dp0.\pkgs\gnuwin32\make-3.81-dep.zip"
)

:INSTALL
REM ### extract the files
if not exist "%~dp0.\var\gnuwin32" mkdir "%~dp0.\var\gnuwin32"
if not exist "%~dp0.\var\gnuwin32\bin\make.exe" (
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-extract-zip.ps1" "%~dp0.\pkgs\gnuwin32\make-3.81-bin.zip" "%~dp0.\tmp\gnuwin32"
    robocopy /move /e "%~dp0.\tmp\gnuwin32" "%~dp0.\var\gnuwin32" /NFL /NDL /NP /NJH /NJS
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-extract-zip.ps1" "%~dp0.\pkgs\gnuwin32\make-3.81-dep.zip" "%~dp0.\tmp\gnuwin32"
    robocopy /move /e "%~dp0.\tmp\gnuwin32" "%~dp0.\var\gnuwin32" /NFL /NDL /NP /NJH /NJS
)

endlocal

REM ### Add make.exe to the system path
if not defined TOD_GNU_MAKE_INSTALLED set PATH=%~dp0var\gnuwin32\bin;%PATH%
if not defined TOD_GNU_MAKE_INSTALLED echo GNU Make 3.81 is installed at .\var\gnuwin32\bin\make.exe
if not defined TOD_GNU_MAKE_INSTALLED set TOD_GNU_MAKE_INSTALLED=1

REM ### Configure GNU Make for parallel processing
REM ### http://dannythorpe.com/2008/03/06/parallel-make-in-win32/
set SHELL=cmd.exe

:END
