@echo off
setlocal enableextensions enabledelayedexpansion 

set OD=%CD%

REM ### Install and configure the tools that we will use for this build
call "%~dp0.\wget-install.cmd"
set PATH=%~dp0.\var\gnuwin32\bin;%PATH%
call "%~dp0.\7zip-install.cmd"
set PATH=%~dp0.\var\7z;%PATH%
call "%~dp0.\msysgit-install.cmd"
set PATH=%~dp0.\var\msysgit\bin;%PATH%
call "%~dp0.\cmake-install.cmd"
set PATH=%~dp0.\var\cmake\bin;%PATH%

REM ### We need MSBuild too
set Path=%Path%;%SystemRoot%\Microsoft.NET\Framework\v4.0.30319\

if not exist "%~dp0.\pkgs" mkdir "%~dp0.\pkgs"
if not exist "%~dp0.\pkgs\emgucv" mkdir "%~dp0.\pkgs\emgucv"
if not exist "%~dp0.\var" mkdir "%~dp0.\var"
if not exist "%~dp0.\var\emgucv" mkdir "%~dp0.\var\emgucv"

REM ### Download EmguCV x64.
IF NOT EXIST "%~dp0.\pkgs\emgucv\libemgucv-windows-x64-gpu-2.4.2.1777.zip" (
    wget -nv "http://sourceforge.net/projects/emgucv/files/emgucv/2.4.2/libemgucv-windows-x64-gpu-2.4.2.1777.zip/download" -O libemgucv-x64.zip || goto :ERROR
    mv libemgucv-x64.zip "%~dp0.\pkgs\emgucv\libemgucv-windows-x64-gpu-2.4.2.1777.zip"
)

REM ### Download EmguCV x86.
IF NOT EXIST "%~dp0.\pkgs\emgucv\libemgucv-windows-x86-gpu-2.4.2.1777.zip" (
    wget -nv "http://sourceforge.net/projects/emgucv/files/emgucv/2.4.2/libemgucv-windows-x86-gpu-2.4.2.1777.zip/download" -O libemgucv-x86.zip || goto :ERROR
    mv libemgucv-x86.zip "%~dp0.\pkgs\emgucv\libemgucv-windows-x86-gpu-2.4.2.1777.zip"
)

REM ### Extract EmguCV x64
if exist "%~dp0.\tmp\emgucv" rmdir /s /q "%~dp0.\tmp\emgucv"
IF NOT EXIST "%~dp0.\var\emgucv\x64" (
    if not exist "%~dp0.\tmp" mkdir "%~dp0.\tmp"
    if not exist "%~dp0.\tmp\emgucv" mkdir "%~dp0.\tmp\emgucv"
    7za.exe x -y "%~dp0.\pkgs\emgucv\libemgucv-windows-x64-gpu-2.4.2.1777.zip" -o"%~dp0.\tmp\emgucv\x64" > 7z.log || goto :ERROR
    mv "%~dp0.\tmp\emgucv\x64" "%~dp0.\var\emgucv\x64"
)

REM ### Extract EmguCV x86
if exist "%~dp0.\tmp\emgucv" rmdir /s /q "%~dp0.\tmp\emgucv"
IF NOT EXIST "%~dp0.\var\emgucv\x86" (
    if not exist "%~dp0.\tmp" mkdir "%~dp0.\tmp"
    if not exist "%~dp0.\tmp\emgucv" mkdir "%~dp0.\tmp\emgucv"
    7za.exe x -y "%~dp0.\pkgs\emgucv\libemgucv-windows-x86-gpu-2.4.2.1777.zip" -o"%~dp0.\tmp\emgucv\x86" > 7z.log || goto :ERROR
    mv "%~dp0.\tmp\emgucv\x86" "%~dp0.\var\emgucv\x86"
)

REM ### Clean up after ourselves
if exist "%~dp0.\tmp\emgucv" rmdir /s /q "%~dp0.\tmp\emgucv"

:END
cd /d "%OD%"
endlocal
exit /b 0

:ERROR
echo the last command terminated with error %errorlevel%
exit /b %errorlevel%

:EOF
