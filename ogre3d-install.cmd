@echo off
setlocal enableextensions enabledelayedexpansion 

set OD = %CD%

REM ### Install and configure 7-zip and wget
call "%~dp0.\wget-install.cmd"
call "%~dp0.\7zip-install.cmd"
set PATH=%~dp0.\var\7z;%~dp0.\var\gnuwin32\bin;%PATH%

REM ### We need MSBuild too
set Path=%Path%;%SystemRoot%\Microsoft.NET\Framework\v4.0.30319\

if not exist "%~dp0.\pkgs" mkdir "%~dp0.\pkgs"
if not exist "%~dp0.\pkgs\ogre3d" mkdir "%~dp0.\pkgs\ogre3d"

:DOWNLOADDEPENDENCIES
REM ### Download the dependencies archive.
REM ### http://www.ogre3d.org/forums/viewtopic.php?f=1&t=54533
IF EXIST "%~dp0.\pkgs\ogre3d\dependencies.zip" GOTO :DOWNLOADSOURCE
wget -nv "https://sourceforge.net/projects/ogre/files/ogre-dependencies-vc%2B%2B/1.9/OgreDependencies_MSVC_20120819.zip/download" -O dependencies.zip || goto :ERROR
mv dependencies.zip "%~dp0.\pkgs\ogre3d\dependencies.zip"

:DOWNLOADSOURCE
REM ### Download the source from BitBucket.
IF EXIST "%~dp0.\pkgs\ogre3d\ogre-src-1.8.1.zip" GOTO :DOWNLOADGLESDEPS
REM ### This is the latest commit on the 1-8-1 tag.
wget -nv https://bitbucket.org/sinbad/ogre/get/525a7f3.zip -O ogre-src-1.8.1.zip || goto :ERROR
mv ogre-src-1.8.1.zip "%~dp0.\pkgs\ogre3d\ogre-src-1.8.1.zip" 

:DOWNLOADGLESDEPS
if EXIST "%~dp0.\pkgs\ogre3d\gles-dependencies.zip" GOTO :EXTRACTFILES
wget -nv "https://sourceforge.net/projects/ogre/files/ogre-dependencies-vc%2B%2B/1.7/OgreDependencies_GLES2_WIN32.zip/download" -O gles-dependencies.zip || goto :ERROR
mv gles-dependencies.zip "%~dp0.\pkgs\ogre3d\gles-dependencies.zip" 

:EXTRACTFILES
if not exist "%~dp0.\src" mkdir "%~dp0.\src"
if not exist "%~dp0.\src\ogre3d" mkdir "%~dp0.\src\ogre3d"
cd /d "%~dp0.\src\ogre3d"
IF EXIST ogre-1.8.1 GOTO :BUILDDEPENDENCIES
REM ### Extract the source
7za.exe x -y "%~dp0.\pkgs\ogre3d\ogre-src-1.8.1.zip" > 7z.log || goto :ERROR
ren "sinbad-ogre-525a7f3bcd4e" ogre-1.8.1
REM ### Extract the dependencies
7za.exe x -y "%~dp0.\pkgs\ogre3d\dependencies.zip" -o"ogre-1.8.1" > 7z.log || goto :ERROR
7za.exe x -y "%~dp0.\pkgs\ogre3d\gles-dependencies.zip" -o"ogre-1.8.1" > 7z.log || goto :ERROR

:BUILDDEPENDENCIES
IF EXIST ogre-1.8.1\Dependencies\src\x64\Release GOTO :END
msbuild /m:4 /p:BuildInParallel=true /nologo /p:Configuration=Release /p:Platform=Win32 /fl /flp:logfile=msbuild-x86-release.log "ogre-1.8.1\Dependencies\src\OgreDependencies.VS2010.sln"
msbuild /m:4 /p:BuildInParallel=true /nologo /p:Configuration=Debug /p:Platform=Win32 /fl /flp:logfile=msbuild-x86-debug.log "ogre-1.8.1\Dependencies\src\OgreDependencies.VS2010.sln"

goto :END
:ERROR
echo the last command terminated with error %errorlevel%
exit /b %errorlevel%

:END

cd /d %OD%
endlocal
:EOF