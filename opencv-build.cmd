@echo off
setlocal enableextensions enabledelayedexpansion 

set OD = %CD%

REM ### Install and configure 7-zip, wget, cmake, touch (gnuwin32-coreutils)
call "%~dp0.\wget-install.cmd"
call "%~dp0.\7zip-install.cmd"
call "%~dp0.\cmake-install.cmd"
call "%~dp0.\gnuwin32-coreutils-install.cmd"
set PATH=%~dp0.\var\7z;%~dp0.\var\gnuwin32\bin;%~dp0.\var\cmake\bin;%PATH%

REM ### We need MSBuild too
set Path=%Path%;%SystemRoot%\Microsoft.NET\Framework\v4.0.30319\

if not exist "%~dp0.\pkgs" mkdir "%~dp0.\pkgs"
if not exist "%~dp0.\pkgs\opencv" mkdir "%~dp0.\pkgs\opencv"

REM ### Download the source code archive.
IF NOT EXIST "%~dp0.\pkgs\opencv\opencv-2.4.6.1.zip" (
    wget --no-check-certificate -nv "https://github.com/Itseez/opencv/archive/2.4.6.1.zip" -O opencv.zip || goto :ERROR
    mv opencv.zip "%~dp0.\pkgs\opencv\opencv-2.4.6.1.zip"
)

REM ### Extract the source code
IF NOT EXIST "%~dp0.\src\opencv" (
    7za.exe x -y "%~dp0.\pkgs\opencv\opencv-2.4.6.1.zip" -o"%~dp0.\src\opencv" > 7z.log || goto :ERROR
)

REM ### Build x64
IF NOT EXIST "%~dp0.\src\opencv\x64\success" (
    mkdir "%~dp0.\src\opencv\x64"
    cd /d "%~dp0.\src\opencv\x64"
    cmake.exe -D:CMAKE_BUILD_TYPE=RELEASE -G "Visual Studio 11 Win64" "%~dp0.\src\opencv\opencv-2.4.6.1" || goto :ERROR
    msbuild.exe /m:4 /p:BuildInParallel=true /nologo /p:Configuration=Release /p:Platform=x64 /fl /flp:logfile=msbuild.log "OpenCV.sln" || goto :ERROR
    touch.exe "%~dp0.\src\opencv\x64\success" 
)

REM ### Build x86
IF NOT EXIST "%~dp0.\src\opencv\x86\success" (
    mkdir "%~dp0.\src\opencv\x86"
    cd /d "%~dp0.\src\opencv\x86"
    cmake.exe -D:CMAKE_BUILD_TYPE=RELEASE -G "Visual Studio 11" "%~dp0.\src\opencv\opencv-2.4.6.1" || goto :ERROR
    msbuild.exe /m:4 /p:BuildInParallel=true /nologo /p:Configuration=Release /p:Platform=Win32 /fl /flp:logfile=msbuild.log "OpenCV.sln" || goto :ERROR
    touch.exe "%~dp0.\src\opencv\x86\success"
)

:END
cd /d "%OD%"
endlocal
exit /b 0

:ERROR
echo the last command terminated with error %errorlevel%
exit /b %errorlevel%

:EOF
