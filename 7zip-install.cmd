@echo off
setlocal enableextensions enabledelayedexpansion 
REM ### This script downloads and installs 7-zip 9.20
REM ### And makes it locally available
REM ### http://www.7-zip.org/

REM ### Download package
if not exist "%~dp0.\pkgs" mkdir "%~dp0.\pkgs"
if not exist "%~dp0.\pkgs\7z" mkdir "%~dp0.\pkgs\7z"
if not exist "%~dp0.\pkgs\7z\7za920.zip" (
    echo Downloading 7-zip 9.20
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-download.ps1" "https://downloads.sourceforge.net/project/sevenzip/7-Zip/9.20/7za920.zip" "%~dp0.\pkgs\7z\7za920.zip"
)

REM ### Extract files
if not exist "%~dp0.\tmp" mkdir "%~dp0.\tmp"
if not exist "%~dp0.\var" mkdir "%~dp0.\var"
if not exist "%~dp0.\var\7z\7za.exe" (
    echo Installing 7-zip 9.20
    if exist "%~dp0.\tmp\7z" rmdir /S /Q "%~dp0.\tmp\7z"
    if exist "%~dp0.\var\7z" rmdir /S /Q "%~dp0.\var\7z"
    mkdir "%~dp0.\tmp\7z"
    mkdir "%~dp0.\var\7z"
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-extract-zip.ps1" "%~dp0.\pkgs\7z\7za920.zip" "%~dp0.\tmp\7z"
    robocopy /move /e "%~dp0.\tmp\7z" "%~dp0.\var\7z" /NFL /NDL /NP /NJH /NJS
    if exist "%~dp0.\tmp\7z" rmdir /S /Q "%~dp0.\tmp\7z"
)

endlocal

REM ### Make 7za.exe locally available on the system path
if not defined TOD_7Z_INSTALLED set PATH=%~dp0var\7z;%PATH%
if not defined TOD_7Z_INSTALLED echo 7-zip 9.20 is installed at .\var\7z\7za.exe
if not defined TOD_7Z_INSTALLED set TOD_7Z_INSTALLED=1