@echo off
setlocal enableextensions enabledelayedexpansion 
REM ### This script downloads and installs wget
REM ### http://gnuwin32.sourceforge.net/packages/wget.htm

:DOWNLOAD
REM ### download the files
if not exist "%~dp0.\pkgs\gnuwin32" mkdir "%~dp0.\pkgs\gnuwin32"
if not exist "%~dp0.\pkgs\gnuwin32\wget-1.11.4-1-bin.zip" (
    echo Downloading WGet
    call "tod-exec-ps.cmd" "tod-download.ps1" "http://downloads.sourceforge.net/gnuwin32/wget-1.11.4-1-bin.zip" "%~dp0.\pkgs\gnuwin32\wget-1.11.4-1-bin.zip"
)
if not exist "%~dp0.\pkgs\gnuwin32\wget-1.11.4-1-dep.zip" (
    echo Downloading WGet Dependencies
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-download.ps1" "https://downloads.sourceforge.net/project/gnuwin32/wget/1.11.4-1/wget-1.11.4-1-dep.zip" "%~dp0.\pkgs\gnuwin32\wget-1.11.4-1-dep.zip"
)

:INSTALL
REM ### extract the files
if not exist "%~dp0.\var\gnuwin32" mkdir "%~dp0.\var\gnuwin32"
if not exist "%~dp0.\var\gnuwin32\bin\wget.exe" (
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-extract-zip.ps1" "%~dp0.\pkgs\gnuwin32\wget-1.11.4-1-bin.zip" "%~dp0.\tmp\gnuwin32"
    robocopy /move /e "%~dp0.\tmp\gnuwin32" "%~dp0.\var\gnuwin32" /NFL /NDL /NP /NJH /NJS
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-extract-zip.ps1" "%~dp0.\pkgs\gnuwin32\wget-1.11.4-1-dep.zip" "%~dp0.\tmp\gnuwin32"
    robocopy /move /e "%~dp0.\tmp\gnuwin32" "%~dp0.\var\gnuwin32" /NFL /NDL /NP /NJH /NJS
)

endlocal

REM ### Add wget.exe to the system path
if not defined TOD_WGET_INSTALLED set PATH=%~dp0var\gnuwin32\bin;%PATH%
if not defined TOD_WGET_INSTALLED echo Wget is installed at .\var\gnuwin32\bin\wget.exe
if not defined TOD_WGET_INSTALLED set TOD_WGET_INSTALLED=1

:END
