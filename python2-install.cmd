@echo off
setlocal enableextensions enabledelayedexpansion 

REM ### This script downloads and installs Python 2.7 
REM ### We have ActiveState to thank for providing this distribution.
REM ### ActiveState ActivePython 

REM ### This script does not constitute redistribution of the software. 
REM ### It only provides the commands required for downloading it.
REM ### You are responsible for reading and complying with the terms of the license
REM ### http://www.activestate.com/activepython/license-agreement

REM ### See Also:
REM ### http://www.activestate.com/
REM ### http://www.activestate.com/activepython
REM ### http://downloads.activestate.com/ActivePython/releases/2.7.2.5/

REM ### Install prerequisites
call "%~dp0.\wget-install.cmd"
set PATH=%~dp0.\var\gnuwin32\bin;%PATH%
call "%~dp0.\7zip-install.cmd"
set PATH=%~dp0.\var\7z;%PATH%

REM ### Package directories
if not exist "%~dp0.\pkgs" mkdir "%~dp0.\pkgs"
if not exist "%~dp0.\pkgs\python2" mkdir "%~dp0.\pkgs\python2"

REM ### Download the distribution
IF NOT EXIST "%~dp0.\pkgs\python2\ActivePython-2.7.2.5-win32-x86.zip" (
    if not exist "%~dp0.\tmp" mkdir "%~dp0.\tmp"
    if not exist "%~dp0.\tmp\python2" mkdir "%~dp0.\tmp\python2"
    wget --no-check-certificate -nv "http://downloads.activestate.com/ActivePython/releases/2.7.2.5/ActivePython-2.7.2.5-win32-x86.zip" -O "%~dp0.\tmp\python2\ActivePython-2.7.2.5-win32-x86.zip" || goto :ERROR
    mv "%~dp0.\tmp\python2\ActivePython-2.7.2.5-win32-x86.zip" "%~dp0.\pkgs\python2\ActivePython-2.7.2.5-win32-x86.zip"
    echo You are solely responsible for reading and complying with the terms of the license agreement as set out by ActiveState
    echo http://www.activestate.com/activepython/license-agreement
)

REM ### Extract Python
IF NOT EXIST "%~dp0.\var\python2" (
    IF EXIST "%~dp0.\tmp\python2" rmdir /s /q "%~dp0.\tmp\python2"
    7za.exe x -y "%~dp0.\pkgs\python2\ActivePython-2.7.2.5-win32-x86.zip" -o"%~dp0.\tmp\python2" > 7z.log || goto :ERROR
    move "%~dp0.\tmp\python2\ActivePython-2.7.2.5-win32-x86\INSTALLDIR" "%~dp0.\var\python2"
    rmdir /s /q "%~dp0.\tmp\python2"
)

REM ### Print a success message
echo python 2.7 is installed at .\var\python2\python.exe

:END
endlocal
exit /b 0

:ERROR
echo the last command terminated with error %errorlevel%
exit /b %errorlevel%

:EOF
