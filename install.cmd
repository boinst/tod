@echo off

REM ### Install script for TOD.
REM ### 
REM ### 

setlocal enableextensions enabledelayedexpansion 

REM ### If the install script exists locally, execute it
if exist "%~dp0install.ps1" (
    powershell.exe -noprofile -noninteractive -executionpolicy bypass -file "%~dp0install.ps1"
)
REM ### If the install script does not exist locally, execute it from the repository
if not exist "%~dp0install.ps1" (
    powershell.exe -noprofile -noninteractive -executionpolicy unrestricted -command "iex ((new-object net.webclient).DownloadString('https://bitbucket.org/boinst/tod/raw/master/install.ps1'))"
)

endlocal