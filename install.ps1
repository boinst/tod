###                           ###
###      Tools On Demand      ###
### http://ninjility.com/tod/ ###
###    Installation Script    ###
###                           ###

### Download a file
function Download-File { param ([string]$url, [string]$file)
    Write-Host "Downloading $url to $file"
    $downloader = new-object System.Net.WebClient
    $downloader.DownloadFile($url, $file)
}

## Compile a single C# source file into an executable
function Compile { param ([string]$source, [string]$exe)
    Write-Host "Compiling $source to $exe"

    $params = New-Object System.CodeDom.Compiler.CompilerParameters
    $params.ReferencedAssemblies.AddRange($(@("mscorlib.dll", "System.dll", "System.Core.dll", "System.Xml.dll", $([psobject].Assembly.Location))))
    $params.GenerateInMemory = $true
    $params.GenerateExecutable = $true
    $params.OutputAssembly = $exe
    
    $provider = New-Object Microsoft.CSharp.CSharpCodeProvider
  
    $results = $provider.CompileAssemblyFromFile($params, $source)
    if ($results.Errors.Count -gt 0) 
    {
        foreach ($err in $results.Errors) 
        {
            Write-Error $err.ToString()
        }
    }
}

$url = "https://bitbucket.org/boinst/tod/raw/master/tod.cs"
$file = ".\tmp\tod.cs"
$exe = ".\tod.exe"
$tmp = ".\tmp"

### Create the temporary working directory
if (!(test-path $tmp)) { md $tmp } 

### Download the C# source for tod.exe
Download-File $url $file

### Compile the C# source to tod.exe
Compile $file $exe
