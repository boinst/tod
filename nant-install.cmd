@echo off
setlocal enableextensions enabledelayedexpansion 
REM ### This script downloads and installs NAnt 0.92
REM ### http://nant.sourceforge.net/

:DOWNLOAD
REM ### download the files
if not exist "%~dp0.\pkgs\nant" mkdir "%~dp0pkgs\nant"
if not exist "%~dp0.\pkgs\nant\nant-0.92-bin.zip" (
    echo Downloading NAnt 0.92
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-download.ps1" "http://downloads.sourceforge.net/project/nant/nant/0.92/nant-0.92-bin.zip" "%~dp0.\pkgs\nant\nant-0.92-bin.zip"
)

:INSTALL
REM ### extract the files
if not exist "%~dp0.\tmp" mkdir "%~dp0.\tmp"
if not exist "%~dp0.\var\nant\bin\nant.exe" (
    if exist "%~dp0.\var\nant" rmdir /S /Q "%~dp0.\var\nant"
    if exist "%~dp0.\tmp\nant" rmdir /S /Q "%~dp0.\tmp\nant"
    mkdir "%~dp0.\var\nant"
    mkdir "%~dp0.\tmp\nant"
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-extract-zip.ps1" "%~dp0.\pkgs\nant\nant-0.92-bin.zip" "%~dp0.\tmp\nant"
    robocopy /move /e "%~dp0.\tmp\nant\nant-0.92" "%~dp0.\var\nant" /NFL /NDL /NP /NJH /NJS
    rmdir /S /Q "%~dp0.\tmp\nant"
)

endlocal

REM ### Add nant.exe to the system path
if not defined TOD_NANT_INSTALLED set PATH=%~dp0var\nant\bin;%PATH%
if not defined TOD_NANT_INSTALLED echo NAnt 0.92 is installed at .\var\nant\bin\nant.exe
if not defined TOD_NANT_INSTALLED set TOD_NANT_INSTALLED=1

:END
