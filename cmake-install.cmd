@echo off
setlocal enableextensions enabledelayedexpansion 

REM ### Install and configure 7-zip and wget
call "%~dp0.\wget-install.cmd"
call "%~dp0.\7zip-install.cmd"
set PATH=%~dp0.\var\7z;%~dp0.\var\gnuwin32\bin;%PATH%

REM ### Package directories
if not exist "%~dp0.\pkgs" mkdir "%~dp0.\pkgs"
if not exist "%~dp0.\pkgs\cmake" mkdir "%~dp0.\pkgs\cmake"

REM ### Download cmake.
IF NOT EXIST "%~dp0.\pkgs\cmake\cmake-2.8.11.2-win32-x86.zip" (
    wget -nv "http://www.cmake.org/files/v2.8/cmake-2.8.11.2-win32-x86.zip" -O cmake.zip || goto :ERROR
    mv cmake.zip "%~dp0.\pkgs\cmake\cmake-2.8.11.2-win32-x86.zip"
)

REM ### Extract cmake
IF NOT EXIST "%~dp0.\var\cmake" (
    7za.exe x -y "%~dp0.\pkgs\cmake\cmake-2.8.11.2-win32-x86.zip" -o"%~dp0.\tmp\cmake" > 7z.log || goto :ERROR
    mv "%~dp0.\tmp\cmake\cmake-2.8.11.2-win32-x86" "%~dp0.\var\cmake"
    rmdir /s /q "%~dp0.\tmp\cmake"
)

REM ### success message
echo cmake is installed at .\var\cmake\bin\cmake.exe

goto :END
:ERROR
echo the last command terminated with error %errorlevel%
exit /b %errorlevel%

:END

endlocal
exit /b 0
:EOF