@echo off

REM ###
REM ### TOD: Tools On Demand -- version 1.0.0
REM ### exec-ps.cmd
REM ###

setlocal enableextensions enabledelayedexpansion || echo ERROR! >&2 && exit /b 1
if [%1]==[--help] goto :USAGE
if [%1]==[/?] goto :USAGE
if [%1]==[-h] goto :USAGE
if [%1]==[help] goto :USAGE
if [%1]==[] goto :USAGE
goto :MAIN

:USAGE
REM ###
REM ### Provide usage information
REM ###
echo TOD: Tools On Demand -- version 1.0.0
echo exec-ps.cmd executes powershell scripts
echo.
echo Usage:
echo exec-ps.cmd [ps-script] 
exit /b 1

:MAIN
set TARGET=%1

REM ###
if not exist "%TARGET%" (
    echo The powershell script "%TARGET%" does not exist.
    exit /b 1
)

REM ### Build a list of arguments to pass to the tool.
REM ### The first argument is the name of the tool, so we want
REM ### to skip the first argument and pass on the rest.
set TARGET_ARGS=
:BUILD_TARGET_ARGUMENTS
if not [%2]==[] (
    set TARGET_ARGS=%TARGET_ARGS% %~2
    shift
    goto :BUILD_TARGET_ARGUMENTS
)

REM ### Resolve the appropriate script and execute it.
powershell.exe -executionpolicy bypass -file "%TARGET%" %TARGET_ARGS%
goto :END


:END
endlocal
exit /b 0

:EOF
