@echo off
setlocal enableextensions enabledelayedexpansion 

REM ### Install and configure 7-zip and wget
call "%~dp0.\wget-install.cmd"
call "%~dp0.\7zip-install.cmd"
set PATH=%~dp0.\var\7z;%~dp0.\var\gnuwin32\bin;%PATH%

REM ### Package directories
if not exist "%~dp0.\pkgs" mkdir "%~dp0.\pkgs"
if not exist "%~dp0.\pkgs\msysgit" mkdir "%~dp0.\pkgs\msysgit"

REM ### Download msysgit.
IF NOT EXIST "%~dp0.\pkgs\msysgit\PortableGit-1.8.3-preview20130601.7z" (
    wget --no-check-certificate -nv "https://msysgit.googlecode.com/files/PortableGit-1.8.3-preview20130601.7z" -O msysgit.7z || goto :ERROR
    mv msysgit.7z "%~dp0.\pkgs\msysgit\PortableGit-1.8.3-preview20130601.7z"
)

REM ### Extract msysgit
IF NOT EXIST "%~dp0.\var\msysgit" (
    7za.exe x -y "%~dp0.\pkgs\msysgit\PortableGit-1.8.3-preview20130601.7z" -o"%~dp0.\var\msysgit" > 7z.log || goto :ERROR
)

REM ### success message
echo msysgit is installed at .\var\msysgit\bin\git.exe

goto :END
:ERROR
echo the last command terminated with error %errorlevel%
exit /b %errorlevel%

:END

endlocal
exit /b 0
:EOF