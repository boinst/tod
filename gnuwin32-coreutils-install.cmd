@echo off
setlocal enableextensions  enabledelayedexpansion 
REM ### This script downloads and installs gnuwin32 core utils
REM ### http://gnuwin32.sourceforge.net/packages/coreutils.htm

:DOWNLOAD
REM ### download the files
if not exist "%~dp0.\pkgs\gnuwin32" mkdir "%~dp0pkgs\gnuwin32"
if not exist "%~dp0.\pkgs\gnuwin32\coreutils-5.3.0-bin.zip" (
    call "tod-exec-ps.cmd" "tod-download.ps1" "http://downloads.sourceforge.net/project/gnuwin32/coreutils/5.3.0/coreutils-5.3.0-bin.zip" "%~dp0.\pkgs\gnuwin32\coreutils-5.3.0-bin.zip"
)
if not exist "%~dp0.\pkgs\gnuwin32\coreutils-5.3.0-dep.zip" (
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-download.ps1" "http://downloads.sourceforge.net/project/gnuwin32/coreutils/5.3.0/coreutils-5.3.0-dep.zip" "%~dp0.\pkgs\gnuwin32\coreutils-5.3.0-dep.zip"
)

:INSTALL
REM ### extract the files
if not exist "%~dp0.\var\gnuwin32" mkdir "%~dp0.\var\gnuwin32"
if not exist "%~dp0.\var\gnuwin32\bin\mkdir.exe" (
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-extract-zip.ps1" "%~dp0.\pkgs\gnuwin32\coreutils-5.3.0-bin.zip" "%~dp0.\tmp\gnuwin32"
    robocopy /move /e "%~dp0.\tmp\gnuwin32" "%~dp0.\var\gnuwin32" /NFL /NDL /NP /NJH /NJS
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-extract-zip.ps1" "%~dp0.\pkgs\gnuwin32\coreutils-5.3.0-dep.zip" "%~dp0.\tmp\gnuwin32"
    robocopy /move /e "%~dp0.\tmp\gnuwin32" "%~dp0.\var\gnuwin32" /NFL /NDL /NP /NJH /NJS
)

:END
endlocal
