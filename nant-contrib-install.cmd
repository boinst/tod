@echo off
setlocal enableextensions enabledelayedexpansion 
REM ### This script downloads and installs NAnt Contrib 0.92
REM ### http://nant.sourceforge.net/

REM ### Require NAnt
call "%~dp0.\nant-install.cmd"

:DOWNLOAD
REM ### download the files
if not exist "%~dp0.\pkgs\nant" mkdir "%~dp0pkgs\nant"
if not exist "%~dp0.\pkgs\nant\nantcontrib-0.92-bin.zip" (
    echo Downloading NAnt Contrib 0.92
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-download.ps1" "http://downloads.sourceforge.net/project/nantcontrib/nantcontrib/0.92/nantcontrib-0.92-bin.zip" "%~dp0.\pkgs\nant\nantcontrib-0.92-bin.zip"
)

:INSTALL
REM ### extract the files
if not exist "%~dp0.\tmp" mkdir "%~dp0.\tmp"
if not exist "%~dp0.\var\nant\bin\NAnt.Contrib.Tasks.dll" (
    if exist "%~dp0.\tmp\nantcontrib" rmdir /S /Q "%~dp0.\tmp\nantcontrib"
    mkdir "%~dp0.\tmp\nantcontrib"
    call "%~dp0.\tod-exec-ps.cmd" "%~dp0.\tod-extract-zip.ps1" "%~dp0.\pkgs\nant\nantcontrib-0.92-bin.zip" "%~dp0.\tmp\nantcontrib"
    robocopy /move /e "%~dp0.\tmp\nantcontrib\nantcontrib-0.92\bin" "%~dp0.\var\nant\bin" /NFL /NDL /NP /NJH /NJS
    rmdir /S /Q "%~dp0.\tmp\nantcontrib"
)

endlocal

REM ### Report success
if not defined TOD_NANTCONTRIB_INSTALLED echo NAnt Contrib 0.92 is installed at .\var\nant\bin\
if not defined TOD_NANTCONTRIB_INSTALLED set TOD_NANTCONTRIB_INSTALLED=1

:END
